
%%%%%%%%%%%%%%%%%%%%%%% file typeinst.tex %%%%%%%%%%%%%%%%%%%%%%%%%
%
% This is the LaTeX source for the instructions to authors using
% the LaTeX document class 'llncs.cls' for contributions to
% the Lecture Notes in Computer Sciences series.
% http://www.springer.com/lncs       Springer Heidelberg 2006/05/04
%
% It may be used as a template for your own input - copy it
% to a new file with a new name and use it as the basis
% for your article.
%
% NB: the document class 'llncs' has its own and detailed documentation, see
% ftp://ftp.springer.de/data/pubftp/pub/tex/latex/llncs/latex2e/llncsdoc.pdf
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\documentclass[runningheads,a4paper]{llncs}

\usepackage{amssymb}
\setcounter{tocdepth}{3}
\usepackage{graphicx}
\usepackage{fancyvrb}
\usepackage{relsize}
\usepackage{textcomp}
\usepackage{multirow}
\usepackage{booktabs}

\DefineVerbatimEnvironment%
 {dsnippet}{Verbatim}
 {fontfamily=tt,fontsize=\smaller,formatcom=\upshape,frame=single,framesep=1mm}

\usepackage{url}
\urldef{\mailsa}\path|{sferrada, bebustos, ahogan}@dcc.uchile.cl|    
\newcommand{\keywords}[1]{\par\addvspace\baselineskip
\noindent\keywordname\enspace\ignorespaces#1}

\setlength{\textheight}{1.1\textheight}
\setlength{\textwidth}{1.04\textwidth}
\setlength{\voffset}{-0.2in}

\usepackage{xcolor}
\usepackage{hyperref}
\definecolor{dark-blue}{rgb}{0.0,0.0,0.2}
\definecolor{dark-green}{rgb}{0.0,0.2,0.0}
\definecolor{dark-red}{rgb}{0.2,0.0,0.0}
\hypersetup{
    colorlinks, linkcolor={dark-red},
    citecolor={dark-green}, urlcolor={dark-blue},
    pdftitle={IMGpedia: Enriching the Web of Data with Image Content Analysis},    % title
    pdfauthor={Sebastián Ferrada, Benjamin Bustos, Aidan Hogan},     % author
    pdfsubject={AMW 2016},   % subject of the document
    pdfkeywords={Linked Data;} {Multimedia;} {DBpedia;} {IMGpedia;}, % list of keywords
}

\newcommand{\cmtA}[1]{{\color{red}Aidan: #1}}
\pagestyle{empty}
\begin{document}

\mainmatter  % start of an individual contribution

% first the title is needed
\title{IMGpedia: Enriching the Web of Data\\with Image Content Analysis}

% a short form should be given in case it is too long for the running head
%\titlerunning{Lecture Notes in Computer Science: Authors' Instructions}

% the name(s) of the author(s) follow(s) next
%
% NB: Chinese authors should write their first names(s) in front of
% their surnames. This ensures that the names appear correctly in
% the running heads and the author index.
%
\author{Sebasti\'an Ferrada \and Benjamin Bustos \and Aidan Hogan%
%\thanks{TBD.}%
}
%
%\authorrunning{Lecture Notes in Computer Science: Authors' Instructions}
% (feature abused for this document to repeat the title also on left hand pages)

% the affiliations are given next; don't give your e-mail address
% unless you accept that it will be published
\institute{Center for Semantic Web Research\\
Department of Computer Science\\
University of Chile\\
\mailsa
}

%
% NB: a more complex sample for affiliations and the mapping to the
% corresponding authors can be found in the file "llncs.dem"
% (search for the string "\mainmatter" where a contribution starts).
% "llncs.dem" accompanies the document class "llncs.cls".
%

%\toctitle{Lecture Notes in Computer Science}
%\tocauthor{Authors' Instructions}
\maketitle


\begin{abstract}
Linked Data rarely takes into account multimedia content, which forms a central part of the Web. To explore the combination of Linked Data and multimedia, we are developing \textsc{IMGpedia}: we compute content-based descriptors for images used in \textsc{Wikipedia} articles and subsequently propose to link these descriptions with legacy encyclopaedic knowledge-bases such as \textsc{DBpedia} and \textsc{Wikidata}. On top of this extended knowledge-base, our goal is to consider a unified query system that accesses both the encyclopaedic data and the image data. We could also consider enhancing the encyclopaedic knowledge based on rules applied to co-occurring entities in images, or content-based analysis, for example. Abstracting away from \textsc{IMGpedia}, we explore generic methods by which the content of images on the Web can be described in a standard way and can be considered as first-class citizens on the Web of Data, allowing, for example, for combining structured queries with image similarity search. This short paper thus describes ongoing work on \textsc{IMGpedia}, with focus on image descriptors.

%\keywords{DBpedia, image entities, linked-data}
\end{abstract}


\section{Introduction}

\textsc{Wikipedia} centres around the curation of human-readable encyclopaedia articles, where at the time of writing it contains about 38 million articles in almost 200 languages. In terms of structured content, the \textsc{DBpedia} project~\cite{LIJ+14} systematically extracts meta-data from \textsc{Wikipedia} articles and presents it as RDF. Another initiative, called \textsc{Wikidata}~\cite{Wikidata} -- organised by the Wikimedia Foundation -- allows users to directly create and curate machine-readable encyclopaedic entries. In terms of multimedia content, \textsc{Wikimedia Commons}\footnote{\url{https://commons.wikimedia.org}} is a collection of 30 million freely-usable media files (image, audio, video) that are used within \textsc{Wikipedia} articles. Recently, \textsc{DBpedia Commons}~\cite{dbcommons} was released: a knowledge-base that takes the meta-data from each multimedia file's description page -- such as the author, size and licensing of the media  -- and publishes it as RDF. However, none of the existing knowledge-bases in this space describe aspects of the multimedia \textit{content} itself.

Because of this we are developing \textsc{IMGpedia}~\cite{imgpedia}, a new knowledge-base that uses meta-data from \textsc{DBpedia Commons} and enriches it with \textit{visual descriptors} of all images from the \textsc{Wikimedia Commons} dataset. The goal is to allow users to perform ``visuo-semantic queries'' using \textsc{IMGpedia}, e.g., \textit{retrieve a painting for every European painter from the 17\textsuperscript{th}\,century} or \textit{given a photo of yourself, obtain the top-$k$ most similar portraits painted by an American artist}. We could also infer new knowledge about \textsc{DBpedia} entities, given the relations between the images; e.g, if two entities of type \texttt{dbo:Person} share an image (or they are very similar), we could infer, with some given likelihood, that they have met. Our general goal is to investigate methods by which descriptors of the content of images -- not just their meta-data -- can be integrated with the Web of Data in a standard way. 

In previous work~\cite{imgpedia}, we proposed the general goals of \textsc{IMGpedia}. This paper is a brief progress update: currently we are downloading the images from \textsc{Wikimedia}, computing the visual descriptors for each image, and creating the image entities that will be linked with existing encyclopaedic knowledge-bases. We thus propose a set of existing multimedia algorithms for extracting descriptors from images, describe their applications, and provide reference implementations in various programming languages such that they be used as a standard way to (begin to) describe images on the Web of Data. We provide some initial results on computing descriptors for the images of \textsc{Wikimedia Commons}, we describe some of the technical engineering challenges faced while realising \textsc{IMGpedia}, and we outline future plans.

\section{Reference Implementations for Visual Descriptors}

Aside from simply providing the URL of an image, we wish to compute visual descriptors for each image. These descriptors are vectors produced after performing different operations over the matrix of pixels in order to obtain certain features, such as color distribution, shapes and textures. Later, those vectors are stored as a part of a metric space in which similarity search can be performed. We are considering four different image descriptors which we have already implemented in Java, Python and C++, and we have proven to give equivalent results among different languages over a dataset of 2800 Flickr images. The first three descriptors require a preprocessing step, which is to convert the image to greyscale using intensity $Y = 0.299\cdot R + 0.587\cdot G + 0.114\cdot B$, where $R$, $G$ and $B$ are the values of each color channel: red, green and blue, respectively. The four descriptors we compute are:
\begin{description}
\item[Gray Histogram Descriptor:] images are partitioned in a fixed amount of blocks. Per each block a histogram of gray intensity is computed; typically intensity takes 8 bit values. Finally, the descriptor is the concatenation of all histograms.
\item[Oriented Gradients Descriptor:] image gradient is calculated via convolution with Sobel kernels~\cite{sobel}. A threshold is then applied to the gradient, and for those pixels that exceed it, the orientation of the gradient is calculated. Finally, a histogram of the orientations is computed and is used as the descriptor.
\item[Edge Histogram Descriptor:] for each $2\times2$ pixel block, the dominant edge orientation is computed (horizontal, vertical, both diagonals or none), where the descriptor is a histogram of these orientations~\cite{manjunath}.
\item[DeCAF7:] a Caffe neural network pre-trained with the Imagenet dataset~\cite{donahue} is used. To obtain the vector, each image is resized and ten overlapping patches are extracted; each patch is given as input to the neural network and the last self-convolutional layer of the model is extracted as a descriptor, so the final vector is the average of the layers for all the patches~\cite{novak}.
\end{description}

The implementations and their dependency/compilation documentation can be found at \url{https://github.com/scferrada/imgpedia} under GNU GPL license.

\section{Performance Evaluation of Descriptors}

In order to build the knowledge-base, preliminary steps must be done. First of all, it 
is necessary to obtain a local copy of the \textsc{Wikimedia Commons} image dataset so we can keep \textsc{IMGpedia} updated in the future. To do so, \textsc{Wikimedia} provides an \textit{rsync}-protocol server for downloading the $\sim$21TB of images of the dataset. Currently we have downloaded about $\sim$19TB at an average speed of $\sim$550GB per day, corresponding to $\sim$14 million images downloaded from a total of $\sim$16 million. Each image is stored locally, along with its four descriptors. 

We benchmarked the computation of descriptors in order to know the time such extraction will take. Experiments were performed on a machine with Debian 4.1.1, a 2.2 GHz (24 core) Intel(r) Xeon processor, and 120 GB of RAM. We computed the descriptors for a sub-folder of the \textsc{Wikimedia Commons} dataset, containing 57,377 images. The process is comprised of three steps: read the image and load it into memory, extract the descriptor itself and save the vector on disk. Two experiments were performed, one using a single execution thread and the other using 28 threads, where each thread handles an image at a time. The neural network for DeCAF7 was used in CPU mode (rather than GPU mode). 

\begin{table}[t]
\setlength{\tabcolsep}{6pt}
\centering
    \caption{Descriptor calculation benchmark \label{tab:times}}
   
\begin{tabular}{lrr}
\toprule

\multirow{2}{*}{\textbf{Descriptor}} & \multicolumn{2}{c}{\textbf{Average time per image} {[}ms{]}} \\ \cmidrule{2-3} 
                            & \textit{Single Thread}         & \textit{Multithread}         \\ \midrule
GHD                         & 119.4            &   10.4                  \\
HOG                         & 477.7            &   25.7                 \\
EHD                         & 729.1            &  42.1                   \\
DeCAF7                   &  3634.8             &  3809.7              \\ \bottomrule
\end{tabular}
\end{table}
%\vspace*{-\baselineskip}


Results are shown in Table \ref{tab:times}, where we see that DeCAF7 is the most expensive descriptor to run. While we can see an improvement of one order of magnitude in calculation time for the first three descriptors, DeCAF7 does not benefit: the Caffe implementation for neural networks already uses multithreading for its computation on a single image, so assigning different images to different threads gives a slight \textit{overhead} since multiple cores are already being exploited. On the present hardware, we estimate it would take $\sim$1.8 years to run DeCAF7 over all 16 million images; hence, we will have to select a subset of images on which to run this descriptor.

\section{Next Steps}

\paragraph{Linking \textsc{IMGpedia} with \textsc{DBpedia}, \textsc{Wikidata}, etc.}
To complete our knowledge-base and be able to infer new relations, we must combine these visual descriptors with the semantic entities of \textsc{DBpedia} and \textsc{Wikidata}. For this, we must extract the links between the articles and the images that they use since they are not present in \textsc{DBpedia Commons} and only present in \textsc{DBpedia} for some images. This is an ongoing effort in which we are researching the possible options to perform this task: we hope to exploit the \textsc{Wikimedia} dumps, either to derive this information directly, or to parse it from the articles themselves. Subsequent to this, we wish to investigate using descriptor-based user-defined-functions in SPARQL to enable visuo-semantic queries that combine, e.g., image similarity with knowledge-base queries.

\paragraph{Efficient algorithms for finding relations between similar images}
To facilitate querying \textsc{IMGpedia}, we propose to compute static relationships between images; e.g., if two images have a descriptor distance that tends to $0$, then we can state that those images are \texttt{nearCopy} of each other. Other distance-based relations can be computed based on other fixed thresholds, such as \texttt{similar}. The brute-force approach for finding all pairs of images within a given distance takes quadratic comparisons. Thus, the challenge is to do this efficiently, where we propose to explore: building an index structure for similarity search over the dataset; using approximate similarity search algorithms; using self-similarity join techniques; and so forth.

\paragraph{Labelling images for multimedia applications} 
By linking \textsc{IMGpedia} to existing knowledge-bases, we hope to label images with categories, types, entities, etc. Thus \textsc{IMGpedia} could serve as a useful resource for the multimedia community; e.g., since we are using Caffe framework for neural networks to compute DeCAF7~\cite{donahue}, we could train a network using the \textsc{Dbpedia Commons} categories extracted for each image as labels, allowing us to automatically classify new images in the dataset or benchmark performance against other classifiers. We could also label images with the specific entities they contain using \textsc{DBpedia}/\textsc{Wikidata} based on the article(s) in which it appears: we could need to tread careful since, e.g., an image used on an article of a person may not contain that person, but we could consider some heuristics such as the location of the image and some further content analysis.

\section{Conclusions} In this short paper, we have given updates of our ongoing work on \textsc{IMGpedia}. We benchmarked four image descriptors for which reference implementations are made public. We discussed a number of open engineering challenges, as well as some open research questions we would like to study once the knowledge-base is completed.
\\ \\
\textit{Acknowledgments} This work was supported by the Millennium Nucleus Center for Semantic Web Research, Grant \textnumero\,NC120004, and Fondecyt, Grant \textnumero\,11140900. We thank Camila Faundez for her assistance.

\bibliographystyle{splncs03}
\bibliography{paper}

\end{document}
